import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DrawerPanelComponent } from './components/drawer-panel/drawer-panel.component';
import { DrawerContentComponent } from './components/drawer-content/drawer-content.component';
import { PlaceFooterComponent } from './components/shared/place-footer/place-footer.component';
import { PlaceHeaderComponent } from './components/shared/place-header/place-header.component';
import { PlaceContentComponent } from './components/shared/place-content/place-content.component';
import { PlaceComponent } from './components/shared/place/place.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    DrawerPanelComponent,
    DrawerContentComponent,
    PlaceComponent,
    PlaceContentComponent,
    PlaceFooterComponent,
    PlaceHeaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    DrawerPanelComponent,
    DrawerContentComponent,
    PlaceComponent,
    PlaceContentComponent,
    PlaceFooterComponent,
    PlaceHeaderComponent
  ]
})
export class SharedModule { }
