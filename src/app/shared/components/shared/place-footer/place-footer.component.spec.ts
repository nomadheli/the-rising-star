import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceFooterComponent } from './place-footer.component';

describe('PlaceFooterComponent', () => {
  let component: PlaceFooterComponent;
  let fixture: ComponentFixture<PlaceFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaceFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
