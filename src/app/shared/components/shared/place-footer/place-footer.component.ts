import { Component, Input, OnInit } from '@angular/core';
import { PlaceFooter } from 'src/app/models';

@Component({
  selector: 'app-place-footer',
  templateUrl: './place-footer.component.html',
  styleUrls: ['./place-footer.component.scss']
})
export class PlaceFooterComponent implements OnInit {

  @Input() data!: PlaceFooter

  constructor() { }

  ngOnInit(): void {
  }

}
