import { Component, Input, OnInit } from '@angular/core';
import { Place } from 'src/app/models';

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class PlaceComponent implements OnInit {
  @Input() placeData!: Place;

  constructor() { }

  ngOnInit(): void {
  }

}
