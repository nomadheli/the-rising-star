import { Component, Input, OnInit } from '@angular/core';
import { PlaceHeader } from 'src/app/models';

@Component({
  selector: 'app-place-header',
  templateUrl: './place-header.component.html',
  styleUrls: ['./place-header.component.scss']
})
export class PlaceHeaderComponent implements OnInit {

  @Input() data!: PlaceHeader;

  constructor() { }

  ngOnInit(): void {
  }

}
