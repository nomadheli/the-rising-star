import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceContentComponent } from './place-content.component';

describe('PlaceContentComponent', () => {
  let component: PlaceContentComponent;
  let fixture: ComponentFixture<PlaceContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaceContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
