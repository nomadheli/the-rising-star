import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-place-content',
  templateUrl: './place-content.component.html',
  styleUrls: ['./place-content.component.scss']
})
export class PlaceContentComponent implements OnInit {
  @Input() data = {};

  constructor() { }

  ngOnInit(): void {
  }

}
