import { Component, Input, OnInit } from '@angular/core';
import { YourRoomPlace } from 'src/app/constants';

@Component({
  selector: 'app-your-room',
  templateUrl: './your-room.component.html',
  styleUrls: ['./your-room.component.scss']
})
export class YourRoomComponent implements OnInit {

  @Input() placeData = YourRoomPlace

  constructor() { }

  ngOnInit(): void {
  }

}
