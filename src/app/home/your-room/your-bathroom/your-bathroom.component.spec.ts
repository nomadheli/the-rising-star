import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourBathroomComponent } from './your-bathroom.component';

describe('YourBathroomComponent', () => {
  let component: YourBathroomComponent;
  let fixture: ComponentFixture<YourBathroomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YourBathroomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YourBathroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
