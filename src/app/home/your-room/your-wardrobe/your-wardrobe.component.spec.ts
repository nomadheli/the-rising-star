import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourWardrobeComponent } from './your-wardrobe.component';

describe('YourWardrobeComponent', () => {
  let component: YourWardrobeComponent;
  let fixture: ComponentFixture<YourWardrobeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YourWardrobeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YourWardrobeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
