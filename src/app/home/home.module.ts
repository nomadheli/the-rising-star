import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { DrawingRoomComponent } from './drawing-room/drawing-room.component';
import { SisterRoomComponent } from './sister-room/sister-room.component';
import { YourRoomComponent } from './your-room/your-room.component';
import { KitchenComponent } from './kitchen/kitchen.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { YourBathroomComponent } from './your-room/your-bathroom/your-bathroom.component';
import { YourWardrobeComponent } from './your-room/your-wardrobe/your-wardrobe.component';
import { LaptopModule } from '../laptop/laptop.module';


@NgModule({
  declarations: [
    DrawingRoomComponent,
    SisterRoomComponent,
    YourRoomComponent,
    KitchenComponent,
    HomeComponent,
    YourBathroomComponent,
    YourWardrobeComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    LaptopModule
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
