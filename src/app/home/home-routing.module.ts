import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DesktopScreenComponent } from '../laptop/desktop-screen/desktop-screen.component';
import { DrawingRoomComponent } from './drawing-room/drawing-room.component';
import { HomeComponent } from './home/home.component';
import { KitchenComponent } from './kitchen/kitchen.component';
import { SisterRoomComponent } from './sister-room/sister-room.component';
import { YourRoomComponent } from './your-room/your-room.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'home',
        component: DrawingRoomComponent
      },
      {
        path: 'home/kitchen',
        component: KitchenComponent
      },
      {
        path: 'home/your-room',
        component: YourRoomComponent,
      },
      {
        path: 'home/your-room/laptop',
        component: DesktopScreenComponent
      },
      {
        path: 'home/sister-room',
        component: SisterRoomComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
