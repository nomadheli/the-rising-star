import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingRoomComponent } from './drawing-room.component';

describe('DrawingRoomComponent', () => {
  let component: DrawingRoomComponent;
  let fixture: ComponentFixture<DrawingRoomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrawingRoomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
