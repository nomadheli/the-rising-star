import { Component, Input, OnInit } from '@angular/core';
import { DrawingRoomPlace } from 'src/app/constants';

@Component({
  selector: 'app-drawing-room',
  templateUrl: './drawing-room.component.html',
  styleUrls: ['./drawing-room.component.scss']
})
export class DrawingRoomComponent implements OnInit {

  @Input() placeData = DrawingRoomPlace;

  constructor() { }

  ngOnInit(): void {
  }

}
