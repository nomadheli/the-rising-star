import { Component, Input, OnInit } from '@angular/core';
import { KitchenPlace } from 'src/app/constants';

@Component({
  selector: 'app-kitchen',
  templateUrl: './kitchen.component.html',
  styleUrls: ['./kitchen.component.scss']
})
export class KitchenComponent implements OnInit {

  @Input() placeData = KitchenPlace

  constructor() { }

  ngOnInit(): void {
  }

}
