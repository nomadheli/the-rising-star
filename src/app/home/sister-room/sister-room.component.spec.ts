import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SisterRoomComponent } from './sister-room.component';

describe('SisterRoomComponent', () => {
  let component: SisterRoomComponent;
  let fixture: ComponentFixture<SisterRoomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SisterRoomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SisterRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
