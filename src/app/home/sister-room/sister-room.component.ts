import { Component, Input, OnInit } from '@angular/core';
import { SisterRoomPlace } from 'src/app/constants';

@Component({
  selector: 'app-sister-room',
  templateUrl: './sister-room.component.html',
  styleUrls: ['./sister-room.component.scss']
})
export class SisterRoomComponent implements OnInit {

  @Input() placeData = SisterRoomPlace

  constructor() { }

  ngOnInit(): void {
  }

}
