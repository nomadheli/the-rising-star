import { Place } from "../models";


export const KitchenPlace: Place = {
  header: {
    name: 'Kitchen'
  },
  content: {

  },
  footer: {
    links: [
      {
        name: 'Go Out',
        url: '/home'
      }
    ]
  }
}

export const DrawingRoomPlace: Place = {
  header: {
    name: 'Drawing Room'
  },
  content: {

  },
  footer: {
    links: [
      {
        name: 'Your Room',
        url: '/home/your-room'
      },
      {
        name: 'Sister-In-Law Room',
        url: '/home/sister-room'
      },
      {
        name: 'Kitchen',
        url: '/home/kitchen'
      }
    ]
  }
}

export const YourRoomPlace: Place = {
  header: {
    name: 'Your Room'
  },
  content: {

  },
  footer: {
    links: [
      {
        name: 'Laptop',
        url: '/home/your-room/laptop'
      },
      {
        name: 'Go Out',
        url: '/home'
      }
    ]
  }
}

export const SisterRoomPlace: Place = {
  header: {
    name: 'Sister-In-Law Room'
  },
  content: {

  },
  footer: {
    links: [
      {
        name: 'Go Out',
        url: '/home'
      }
    ]
  }
}

export const YourLaptopPlace: Place = {
  header: {
    name: 'Laptop'
  },
  content: {

  },
  footer: {
    links: [
      {
        name: 'Close',
        url: '/home'
      }
    ]
  }
}

