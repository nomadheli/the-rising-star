import { addYears } from 'date-fns';

let age = 18;

export const player = {
  firstName: 'Alex',
  lastName: 'Costa',
  name: 'Alex Costa',
  age: age,
  dob: addYears(new Date(), -age),
  chest: 34,
  waist: 28,
  hips: 34,
  feminity: 0,
  bodyParts: {

  },
  clothes: {
    shirt: { on: true, type: 1 },
    pant: { on: true, type: 1 },
    undi: { on: true, type: 1 },
    socks: { on: true, type: 1 },
    shoes: { on: true, type: 1 },
    bra: { on: false, type: 1 },
    panty: { on: false, type: 1 },
    top: { on: false, type: 1 },
    bottom: { on: false, type: 1 },
    stocking: { on: false, type: 1 },
    hill: { on: false, type: 1 },
  }
}
