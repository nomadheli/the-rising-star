import { Component, OnInit, Input } from '@angular/core';
import { YourLaptopPlace } from 'src/app/constants';

@Component({
  selector: 'app-desktop-screen',
  templateUrl: './desktop-screen.component.html',
  styleUrls: ['./desktop-screen.component.scss']
})
export class DesktopScreenComponent implements OnInit {

  @Input() placeData = YourLaptopPlace

  constructor() { }

  ngOnInit(): void {
  }

}
