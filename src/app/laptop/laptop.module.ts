import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesktopScreenComponent } from './desktop-screen/desktop-screen.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    DesktopScreenComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    DesktopScreenComponent
  ]
})
export class LaptopModule { }
