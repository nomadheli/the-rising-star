export interface Place {
  header: PlaceHeader,
  content: PlaceContent,
  footer: PlaceFooter
}

export interface PlaceHeader {
  name: string
}

export interface PlaceContent {}

export interface PlaceFooter {
  links: Link[]
}

export interface Link {
  name: string;
  url: string;
}
