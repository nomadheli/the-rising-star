
export interface Cloth {
  id: number,
  img: string,
  price: number,
  type: 'Bra' | 'Panty' | 'Top' | 'Bottom' | 'Stocking' | 'Hill' | 'Shirt' | 'Pant' | 'Socks' | 'Shoes' | 'Undi'
}

export interface Bra extends Cloth {
  type: 'Bra'
}

export interface Panty extends Cloth {
  type: 'Panty'
}

export interface Bottom extends Cloth {
  type: 'Bottom'
}

export interface Top extends Cloth {
  type: 'Top'
}

export interface Stocking extends Cloth {
  type: 'Stocking'
}

export interface Hill extends Cloth {
  type: 'Hill'
}

export interface Wearable {
  on: boolean,
  type: Cloth
}
