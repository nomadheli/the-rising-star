
export interface BodyPart {
  type: 'Chest' | 'Abs' | 'Ass' | 'AHole' | 'Penis' | 'Arms' | 'Legs' | 'Face' | 'Beard',
  isShaved: boolean,
  size?: number,
  shaveDaysCount?: 7,
  shaved: {
    img: ''
  },
  notShaved: {
    img: ''
  }
}
